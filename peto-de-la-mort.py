import random
import os

llista = ["Marta", "Joan", "Maria", "Albert", "Lluís", "Xavier",
          "Irene", "Pau", "Marc", "Roc"]
llista_aleatoria = llista.copy()
random.shuffle(llista_aleatoria)

num_jugadors = len(llista)
parelles = {llista_aleatoria[i]: llista_aleatoria[i+1] for i in range(num_jugadors-1)}
parelles[llista_aleatoria[-1]] = llista_aleatoria[0]

os.system("clear")

entrada = ""
while entrada != "exit":
    print("Aquesta és la llista de jugadors: \n")
    print(llista, "\n")
    jugador = input("Com et dius? ")
    if jugador in parelles:
        entrada = input(f"Jugador {jugador} fes enter.\n")
        print(f"Has de fer-li un petó mortal a en/na {parelles[jugador]}.\n")
        entrada = input("Prem enter per amagar-li al següent jugador.")
        os.system("clear")
    else:
        print("No trobo aquest jugador.\n")


